
// <c>open up your console !            → CTRL+SHIFT+J on chrome</c>
// <c>                                  → CTRL+SHIFT+K on firefox</c>
// <c>                                  → ~ for the coronatime console</c>

// let help = `<JSON style="display:block;margin:auto;width:min-content">
// <div style="display:inline-block;width:100%;text-align: center"><c> ╔╔╦──────────╦╗╗ </c></div>
// <div style="display:inline-block;width:100%;text-align: center"><c>╼│││   HELP   │││╾</c></div>
// <div style="display:inline-block;width:100%;text-align: center"><c> ╚╚╩──────────╩╝╝ </c></div>
// <!-- list()                           <c>→ list all users in all rooms</c> -->
// <!-- room()                           <c>→ list all rooms</c> -->
// login(<s>"player1"</s>)                  <c>→ login as "player1"</c> <!-- logout()
// list(<s>"player5"</s>)                                 <c>// someone</c>
// room(<s></s>)                                          <c>// all rooms</c> -->
// room(<s>"r1"</s>)                        <c>→ go to room "game1", returns current room</c>
// r                                 <c>→ current room (it's a shortcut)</c>
// <!-- room(<s>"r1"</s>).list()                 <c>→ list all users in room "game1"</c>
// room(<s>"r1"</s>).show()                 <c>→ show state of room "game1"</c>
// room(<s>"r1"</s>).close()                <c>→ only if everyone has left</c> -->

// <div style="display:inline-block;width:100%;text-align: center"><c>╭───────────────╮</c></div>
// <div style="display:inline-block;width:100%;text-align: center"><c>│   temporary   │</c></div>
// <div style="display:inline-block;width:100%;text-align: center"><c>╰───────────────╯</c></div>

// r.join()                          <c>→ join timebomb session</c>
// r.leave()                         <c>→ leave timebomb session</c>
// r.start()                         <c>→ start timebomb with players in session</c>
// r.cut(<s>"player8"</s>)                  <c>→ timebomb action </c>

// <div style="display:inline-block;width:100%;text-align: center"><c>╭─────────────────╮</c></div>
// <div style="display:inline-block;width:100%;text-align: center"><c>│   coming soon   │</c></div>
// <div style="display:inline-block;width:100%;text-align: center"><c>╰─────────────────╯</c></div>

// r.card(<s>"name"</s>)                    <c>→ access or create a card</c>

// r.card(<s>"name"</s>).setF(<s>"url/f.jpg"</s>)  <c>→ set texture front</c>
// r.card(<s>"name"</s>).setB(<s>"url/b.jpg"</s>)  <c>→ set texture back</c>
// r.card(<s>"name"</s>).setH(<s>"King[5]"</s>)    <c>→ set text/HTML</c>
// r.card(<s>"name"</s>).size(<i>w</i>,<i>h</i>)          <c>→ set size</c>

// r.card(<s>"name"</s>).move(<i>x</i>,<i>y</i>)          <c>→ set position (deck?)</c>
// r.card(<s>"name"</s>).flip()             <c>→ flip card</c>
// r.card(<s>"name"</s>).peak()             <c>→ peak card</c>

// <!--
// r.deck(<s>"name"</s>)           <c>→ access or create a deck</c>
// r.deck(<s>"name"</s>).eat(...)  <c>→ put a card in there</c>
// r.deck(<s>"name"</s>).shuffle() <c>→ shuffle</c>
// r.deck(<s>"name"</s>).handout([<s>"player1"</s>,<s>"player5"</s>])

// r.dice(<s>"name"</s>)           <c>→ access or create a dice</c>
// r.dice(<s>"name"</s>).roll(<i>6</i>)   <c>→ roll a number between 1 and 6</c>

// r.undo()                 <c>→ revert last action</c>
// r.show(<i>-3</i>)               <c>→ show 3 moves before</c>


// catalog                      <c>→ all avaiable games</c>
// room(<s>"game1"</s>).game(catalog.timebomb).join()         <c>→ join timebomb session</c>
// room(<s>"game1"</s>).game(catalog.timebomb).leave()        <c>→ leave timebomb session</c>
// room(<s>"game1"</s>).game(catalog.timebomb).start()        <c>→ start timebomb with players in session</c>
// room(<s>"game1"</s>).game(catalog.timebomb).cut(<s>"player8"</s>) <c>→ timebomb action </c>
//  -->
// </JSON>`


// let credits = `<div class="monospace" style="text-align: center;">
// <grey> ╔╔╦─────────────╦╗╗ </grey>
// <grey>╼│││   CREDITS   │││╾</grey>
// <grey> ╚╚╩─────────────╩╝╝ </grey>

// <grey>╭──────────────╮</grey>
// <grey>│   MAIN DEV   │</grey>
// <grey>╰──────────────╯</grey>

// Laurent Rohrbasser

// <grey>╭──────────────────╮</grey>
// <grey>│   CONTRIBUTION   │</grey>
// <grey>╰──────────────────╯</grey>

// Mario Geiger
// Robin Szymczak
// Robin Nigon
// Camilo Pineda Serna

// <grey>╭───────────╮</grey>
// <grey>│   TESTS   │</grey>
// <grey>╰───────────╯</grey>

// Anne Sophie Ollivier
// Mathilde
// Brahim
// </div>`

// let welcome = `<div style='text-align:center;'>It's<orange style="line-height:16px;font-size:16px">

// ╔══╦════╗╔═══════╗╔═══════╗ ╔════╤══╗╔═══╗ ╔══╗╔═══════╗╔═══════╗╔═══╗╔═══╗ ╔═══╗╔═══════╗
// │ ╾╨╮   ││       ││   ╔═╗ │ │    ╰╼ ││   ╚╗║  ││  ╔═╗  ││       ││   ││   ╚═╝   ││   ╔═══╝
// │       ││    ┴┭╮││   ╚═╝ ╚╗│       ││    ╚╝  ││  ╚═╝  │╚═╗   ╔═╝│   ││         ││   ╚══╗ 
// │     ╔═╝│  ╔═╗ ╿││   ╔══╗ ││  ╔═╗  ││  ╔╗    ││       │  │   │  │ ┭╮││  ╔╗ ╔╗  ││   ╔══╝ 
// │     ╚═╗│  ╚═╝ ┆││   ║  ║ ││  ╚═╝  ││  ║╚╗   ││  ╔═╗  │  │   │  │  ╿││  ║╚═╝║  ││   ╚═══╗
// ╚═══╧═══╝╚══════╧╝╚═══╝  ╚═╝╚═══════╝╚══╝ ╚═══╝╚══╝ ╚══╝  ╚═══╝  ╚══╧╝╚══╝   ╚══╝╚═══════╝
// </orange></div>
// `


let welcome = `<div style='text-align:center;'>It's<orange style="line-height:16px;font-size:16px">

╔══╦════╗╔═══════╗╔═══════╗ ╔════╤══╗╔═══╗ ╔══╗╔═══════╗
│ ╾╨╮   ││       ││   ╔═╗ │ │    ╰╼ ││   ╚╗║  ││  ╔═╗  │
│       ││    ┴┭╮││   ╚═╝ ╚╗│       ││    ╚╝  ││  ╚═╝  │
│     ╔═╝│  ╔═╗ ╿││   ╔══╗ ││  ╔═╗  ││  ╔╗    ││       │
│     ╚═╗│  ╚═╝ ┆││   ║  ║ ││  ╚═╝  ││  ║╚╗   ││  ╔═╗  │
╚═══╧═══╝╚══════╧╝╚═══╝  ╚═╝╚═══════╝╚══╝ ╚═══╝╚══╝ ╚══╝
╔═══════╗╔═══╗╔═══╗ ╔═══╗╔═══════╗
│       ││   ││   ╚═╝   ││   ╔═══╝
╚═╗   ╔═╝│   ││         ││   ╚══╗ 
  │   │  │ ┭╮││  ╔╗ ╔╗  ││   ╔══╝ 
  │   │  │  ╿││  ║╚═╝║  ││   ╚═══╗
  ╚═══╝  ╚══╧╝╚══╝   ╚══╝╚═══════╝
</orange></div>
`














