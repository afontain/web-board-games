let {range,shuffle} = require('./tools.js')

let orangify = str => '<orange>'+str+'</orange>'

class Coup {

	gamedata = {}
	get g() {return this.gamedata} // a shortcut
	set g(gamedata) {this.gamedata = gamedata} // a shortcut

	// handout(cardsToHandOut) {
	// 	this.g.hands = this.g.hands || {}
	// 	for(let p of this.g.players) this.g.hands[p] = this.g.hands[p] || []
	// 	cardsToHandOut.map( (c,i) => this.g.hands[this.g.players[i % this.g.players.length]].push(c))
	// }

	constructor(user) {

		this.show = user => user.execClient('gui','')
		// this.cut  = user => user.execClient('log',"la partie n'a pas commencé")
		// this.list = user => user.execClient('log',this.g.players?`${this.g.players} sont en jeu`:`${user.roommateplayernames} pourraient jouer`)

		this.start = user => {
			user.roommateExecClient('log',orangify(user.name) + ' is starting timebomb')
			let playerCount = user.roommateplayer.length

			// if(!global.TESTING) {
			// 	if(playerCount<4) {
			// 		user.roommateExecClient('log','need ' + orangify(4-playerCount) + " more players to start (min 4 players)")
			// 		return false
			// 	}
			// 	if(playerCount>8) {
			// 		user.roommateExecClient('log',orangify(playerCount-8) + " players must leave to start (max 8 players)")
			// 		return false
			// 	}
			// }

			this.g = {}
			this.g.players = user.roommateplayer // the game now freezes players playing

			this.show = user => {
				// TODO
				user.execClient('gui',`<coup>NOT IMPLEMENTED</coup>`)
			}

			// user.roommateExecClient('sound','/games/timebomb/fil-merde.mp3')
			user.roommate.map( p => this.show(p))
			user.roommateExecClient('log',`coup started with ${this.g.players.map(p => orangify(p))}`)
			return true

		}
	}
}

exports.Coup = Coup
