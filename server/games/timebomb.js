let {range,shuffle} = require('./tools.js')

let clone = array => array.map(x=>x)
let orangify = str => '<orange>'+str+'</orange>'

class TimeBomb {

	gamedata = {}
	get g() {return this.gamedata} // a shortcut
	set g(gamedata) {this.gamedata = gamedata} // a shortcut

	// remark : we assume that the card count is compatible with player count
	handout(cardsToHandOut) {
		this.g.hands = this.g.hands || {}
		for(let p of this.g.players) this.g.hands[p] = this.g.hands[p] || []
		cardsToHandOut.map( (c,i) => this.g.hands[this.g.players[i % this.g.players.length]].push(c))
	}

	generateRolesPool(playerCount) {
		let good='gentil'
		let evil='méchant'
		switch(playerCount) {
			case 4:
			case 5: return [...new Array(3).fill(good),...new Array(2).fill(evil)]
			case 6: return [...new Array(4).fill(good),...new Array(2).fill(evil)]
			case 7:
			case 8: return [...new Array(5).fill(good),...new Array(3).fill(evil)]
			default: return []
		}
	}

	constructor(user) {
		let filMerde  ='fil de merde'
		let filStyle  ='fil stylé'
		let filBigben ='big ben'

		this.show = user => user.execClient('gui','')
		this.cut  = user => user.execClient('log',"la partie n'a pas commencé")
		// this.list = user => user.execClient('log',this.g.players?`${this.g.players} sont en jeu`:`${user.roommateplayernames} pourraient jouer`)

		this.start = user => {
			user.roommateExecClient('log',orangify(user.name) + ' is starting timebomb')
			let playerCount = user.roommateplayer.length

			if(!global.TESTING) {
				if(playerCount<4) {
					user.roommateExecClient('log','need ' + orangify(4-playerCount) + " more players to start (min 4 players)")
					return false
				}
				if(playerCount>8) {
					user.roommateExecClient('log',orangify(playerCount-8) + " players must leave to start (max 8 players)")
					return false
				}
			}

			this.g = {}
			this.g.players = user.roommateplayer // the game now freezes players playing
			this.g.roles = {}
			this.g.pince = shuffle(this.g.players)[0]
			this.g.defausse = []

			if(global.TESTING) {
				this.g.players = ['laurent','pierre','rosine','christine']
				this.g.pince = shuffle(this.g.players)[0]
				playerCount = this.g.players.length
			}

			let rolePool = shuffle(this.generateRolesPool(this.g.players.length))
			for(let p of this.g.players) this.g.roles[p] = rolePool.pop()

			let cards = shuffle([
				...new Array(playerCount    ).fill(filStyle),
				...new Array(playerCount*4-1).fill(filMerde),
				filBigben
			])
			this.handout(cards)

			// console.log(this.g)

			let hand2html = (user,player) => {
				let pince = `<svg width="100" height="100"><image xlink:href="/games/timebomb/bolt-cutter.svg" width="100%" height="100%"/></svg>`
				// if( global.TESTING || user.name==player) {
				if( user.name==player) {
					let hand = this.g.hands[player].map( (c,i) => {
						let css = ""
						if(c==filBigben) css="tb-bigben"
						if(c==filMerde)  css="tb-merde"
						if(c==filStyle)  css="tb-style"
						return `<card class='${css}' onclick="r.execServer('timebomb.cut',{target:'${player}',index:${i}})"></card>`
					}).join('')

					// let pince = `<svg width="32" height="20" style="margin-bottom:-7px"><image xlink:href="/games/timebomb/bolt-cutter.svg" width="100%" height="100%"/></svg>`
					return `<cardcontainer><div>${player} [${orangify(this.g.roles[player])}]</div>${player==this.g.pince?pince:""}${hand}</cardcontainer>`
					// return `<cardcontainer><div>${player} [${orangify(this.g.roles[player])}]${player==this.g.pince?` [<orange>${pince}</orange>]`:""}</div>${player==this.g.pince?pince:""}${hand}</cardcontainer>`
					// return `<cardcontainer><div>${player} [${orangify(this.g.roles[player])}]${player==this.g.pince?" [<orange>pince</orange>]":""}</div>${hand}</cardcontainer>`

				} else {
					let hand = this.g.hands[player].map( (c,i) => `<card class='tb-unkown' onclick="r.execServer('timebomb.cut',{target:'${player}',index:${i}})"></card>`).join('')
					// return `<cardcontainer><div>${player}${player==this.g.pince?" [<orange>pince</orange>]":""}</div>${hand}</cardcontainer>`
					return `<cardcontainer><div>${player}</div>${player==this.g.pince?pince:""}${hand}</cardcontainer>`
				}
			}

			let stack2html = (cardcss,count) => {
				return `<stacked>${new Array(count).fill(`<card class='${cardcss}'></card>`).join('')}</stacked>${count>1?`x${count}`:''}`
			}

			this.show = user => {
				let playersHand = this.g.players.map( player => hand2html(user,player)).join('')

				let defausse = ""
				defausse += stack2html('tb-bigben',this.g.defausse.filter(c => c==filBigben).length)
				defausse += stack2html('tb-style',this.g.defausse.filter(c => c==filStyle).length)
				defausse += stack2html('tb-merde',this.g.defausse.filter(c => c==filMerde).length)
				defausse = `<cardcontainer class='defausse'><div>defausse</div>${defausse}</container>`

				user.execClient('gui',`<timebomb>${playersHand}${defausse}</timebomb>`)
			}

			this.cut = (user,args) => {
				let targetPuppet = args.target
				let index = args.index
				if(!global.TESTING && user.name!=this.g.pince) {
					user.roommateExecClient('log',`${orangify(user.name)} essaye de couper chez ${orangify(targetPuppet)} sans pince! Quel con!`)
					return
				}
				if(!global.TESTING && user.name==targetPuppet) {
					user.roommateExecClient('log',`${orangify(user.name)} essaye de couper chez lui même! Quel boulet!`)
					return
				}
				if(!this.g.hands[targetPuppet]) {
					user.execClient('log',`pas de ${orangify(targetPuppet)} ici, mais y'a ${this.g.players.map(p=>orangify(p)).join(", ")}`)
					return
				}
				if (index == undefined) index = Math.floor(Math.random()*this.g.hands[targetPuppet].length)
				if(!this.g.hands[targetPuppet][index]) {
					user.execClient('log',`${orangify(targetPuppet)} n'a que ${orangify(this.g.hands[targetPuppet].length)} cartes`)
				}

				let card = this.g.hands[targetPuppet].splice(index,1)[0]
				this.g.pince = targetPuppet
				this.g.defausse.push(card)

				if(card==filBigben) {
					user.roommateExecClient('sfx','/games/timebomb/mechant-win-short.mp3')
					user.roommateExecClient('log',`<red>💥</red> ${orangify(user.name)} a fait péter la bombe! BOUM! <red>💥</red>`)
				}

				let cardLeft = 0
				for(let player of this.g.players) cardLeft += this.g.hands[player].length
				if(cardLeft == this.g.players.length) {
					user.roommateExecClient('sfx','/games/timebomb/mechant-win-short.mp3')
					user.roommateExecClient('log',`<red>💥</red> ${orangify(user.name)} a joué le derier coup, on ne peut plus rien faire! BOUM! <red>💥</red>`)
				}

				if(this.g.defausse.length % this.g.players.length == 0 ) {
					user.roommateExecClient('log',`<red><yellow_b>tour terminé! redistribution des cartes</yellow_b></red>`)
					user.roommateExecClient('sfx','/games/secret-nigon/shuffle.mp3')
					// shuffle et redistribuer
					let cards = []
					for(let player of this.g.players) {
						cards = cards.concat(this.gamedata.hands[player])
						this.gamedata.hands[player] = []
					}
					this.handout(shuffle(cards))
				}

				user.roommateExecClient('log',`${orangify(user.name)} a trouvé ${orangify(card)} chez ${orangify(targetPuppet)}`)
				if(card == filStyle) {
					user.roommateExecClient('sfx','/games/timebomb/fil-style.mp3')
				} else if (card == filMerde) {
					// user.roommateExecClient('sfx','/games/timebomb/fil-merde.mp3')
				}

				let gentilGagne = this.g.defausse.filter(c => c == filStyle).length==this.g.players.length
				if(gentilGagne) {
					user.roommateExecClient('log',`les gentils gagnent !`)
					user.roommateExecClient('sfx','/games/timebomb/gentil-win.mp3')
				}
				user.roommate.map( p => this.show(p))
			}

			user.roommate.map( p => this.show(p))
			user.roommateExecClient('log',`timebomb started with ${this.g.players.map(p => orangify(p))}`)
			return true

		}
	}
}

exports.TimeBomb = TimeBomb
