let {range,shuffle,svg2html} = require('./tools.js')

let clone = array => array.map(x=>x)
let orangify = str => '<orange>'+str+'</orange>'

// TODO : veto
// TODO : inspectPlayer
// TODO : test suite

// TO TEST : refuse temporary president
// TO TEST : veto → random policy ?

// DONE : 5-6 players : nigon sees others
// DONE : 5 players living : presidentPrev can be chosen as chancellor
// DONE : after triple fail : anyone can be chancellor
// TODO : policy peak : tell which will be voted

// question : what happens if temporary is next president ? president twice ?

// TODO : win visual+sfx feedback
// TODO : win on nigon election
// TODO : win on nigon killed

// TODO : sfx : when stage → kill : gun loading
// TODO : sfx : when stage → inspect player : ???
// TODO : sfx : chancello chosen

// TODO : bruitage plus soft

// TODO : anarchiste : parmis les fascistes, l'anarchiste doit buter le nigon

let gameStage = {
	start                     :'start',
	presidentChooseChancellor :'presidentChooseChancellor',
	crewVoting                :'crewVoting',
	crewVoted                 :'crewVoted',
	presidentVoting           :'presidentVoting',
	chancellorVoting          :'chancellorVoting',
	temporaryPresident        :'temporaryPresident',
	inspectPolicies           :'inspectPolicies',
	inspectPlayer             :'inspectPlayer',
	inspectPlayerPhase2       :'inspectPlayerPhase2',
	veto                      :'veto',
	kill                      :'kill',
	end                       :'end',
}

// class SNPlayer {
// 	constructor(role) {
// 		// TODO : check if role is an element of SNPlayer.All
// 		this.role = role
// 	}
// 	getPub(seenBy) {
// 		switch(this.role) {
// 			case SNPlayer.All.nigon:
// 				switch(seenBy.role) {
// 					case SNPlayer.All.nigon:
// 					case SNPlayer.All.nigoniste:
// 					case SNPlayer.All.gentil:
// 					case SNPlayer.All.anarchiste:
// 				}
// 			case SNPlayer.All.nigoniste:
// 				switch(seenBy.role) {
// 					case SNPlayer.All.nigon:
// 					case SNPlayer.All.nigoniste:
// 					case SNPlayer.All.gentil:
// 					case SNPlayer.All.anarchiste:
// 				}
// 			case SNPlayer.All.gentil:
// 				switch(seenBy.role) {
// 					case SNPlayer.All.nigon:
// 					case SNPlayer.All.nigoniste:
// 					case SNPlayer.All.gentil:
// 					case SNPlayer.All.anarchiste:
// 				}
// 			case SNPlayer.All.anarchiste:
// 				switch(seenBy.role) {
// 					case SNPlayer.All.nigon:
// 					case SNPlayer.All.nigoniste:
// 					case SNPlayer.All.gentil:
// 					case SNPlayer.All.anarchiste:
// 				}
// 				break
// 		}
// 	}
// 	getPri()       {return this.role}
// 	static All = {
// 		nigon     : "nigon",
// 		nigoniste : "nigoniste",
// 		gentil    : "gentil",
// 		anarchiste: "anarchiste"
// 	}
// }

class SecretNigon {

	g = {} // gamedata

	// === constants ===
	get gentil()    {return `gentil`}
	get nigoniste() {return `nigoniste`}
	get nigon()     {return `nigon`}
	get anarchiste(){return `anarchiste`}
	get policyR()   {return `loi de ${this.nigoniste}`}
	get policyB()   {return `loi de ${this.gentil}`}

	// === initialization ===
	generatePolicies() {
		return [...range(11).fill(this.policyR),...range(6).fill(this.policyB)]
	}

	generateRoles(playerCount) {
		switch(playerCount) {
			case 5: return [...new Array(3).fill(this.gentil),...new Array(1).fill(this.nigoniste),this.nigon]
			case 6: return [...new Array(4).fill(this.gentil),...new Array(1).fill(this.nigoniste),this.nigon]
			case 7: return [...new Array(4).fill(this.gentil),...new Array(2).fill(this.nigoniste),this.nigon]
			case 8: return [...new Array(5).fill(this.gentil),...new Array(2).fill(this.nigoniste),this.nigon]
			case 9: return [...new Array(5).fill(this.gentil),...new Array(3).fill(this.nigoniste),this.nigon]
			case 10:return [...new Array(6).fill(this.gentil),...new Array(3).fill(this.nigoniste),this.nigon]
			default: return []
		}
	}

	generateBadEvents(playerCount) {
		switch(playerCount) {
			case 5:
			case 6:  return [gameStage.presidentChooseChancellor,gameStage.presidentChooseChancellor,gameStage.inspectPolicies,gameStage.kill,gameStage.kill,gameStage.end]
			case 7:
			case 8:  return [gameStage.presidentChooseChancellor,gameStage.inspectPlayer,gameStage.temporaryPresident,gameStage.kill,gameStage.kill,gameStage.end]
			case 9:
			case 10: return [gameStage.inspectPlayer,gameStage.inspectPlayer,gameStage.temporaryPresident,gameStage.kill,gameStage.kill,gameStage.end]
			default: return []
			// default:
				// return Array(6).fill(gameStage.inspectPlayer)
				// return Array(6).fill(gameStage.inspectPolicies)
				// return Array(6).fill(gameStage.kill)
				// return Array(6).fill(gameStage.temporaryPresident)
		}
	}

	// === helper ===
	isSpectator(user) {
		if(!user.name) return true
		if(this.g.players.filter( u => u == user.name ).length == 0) return true
		return false
	}

	// === action ===
	kill(user,target) {
		if(this.isSpectator(user)) {return} // not for spectator

		this.g.living[target] = !this.g.living[target]

		// let action = this.g.living[target]?`un-killed`:svg2html('/games/secret-nigon/gun-s.svg',32,20,'margin-bottom:-7px')
		if(this.g.living[target]) {
			user.roommateExecClient('sfx',`/games/secret-nigon/crowd-gasp.mp3`)
		} else {
			user.roommateExecClient('sfx',`/games/secret-nigon/cartoon-gun.mp3`)
			setTimeout(() => {
				user.roommateExecClient('sfx',`/games/default/minecraft-ouch.mp3`)
			}, 500)
		}

		let nigonIsDead = false
		for(let p of this.g.players) {
			if(this.g.roles[p]==this.nigon && this.g.living[p]==false) {
				nigonIsDead = true
				break
			}
		}

		if(nigonIsDead) {
			this.g.gameStage = gameStage.end
		} else {
			this.g.gameStage = gameStage.presidentChooseChancellor
			this.autoNextPresident(user)
		}

		// user.roommateExecClient('log',`${orangify(user.name)} ${action} ${orangify(target)}`)
		user.roommate.map( p => this.show(p))
	}

	reshuffleIfNecessary(user) {
		if(this.g.policies.length<3) {
			while(this.g.policiesDropped.length) this.g.policies.push(this.g.policiesDropped.pop())
			this.g.policies = shuffle(this.g.policies)
			setTimeout( () => user.roommateExecClient('sfx',`/games/secret-nigon/shuffle.mp3`), 300)
		}
	}

	returnCards(user) {
		if(this.isSpectator(user)) {return} // not for spectator
		if(this.g.policiesExaminedBy == user.name) {
			while(this.g.policiesExamined.length) this.g.policies.push(this.g.policiesExamined.pop())
			user.roommateExecClient('sfx',`/games/default/card-take-1.mp3`)
			this.g.policiesExaminedBy=undefined
			this.autoNextPresident(user)
			this.g.gameStage = gameStage.presidentChooseChancellor
			user.roommate.map( p => this.show(p))
		}
	}

	checkPlayer(user,target) {
		user.roommateExecClient('log',`${orangify(user.name)} is checking ${orangify(target)}`)
		this.g.gameStage = gameStage.inspectPlayerPhase2
		this.g.checkedPlayer = target
		user.roommate.map( p => this.show(p))
	}

	passTopPolicy(user) {
		if(this.isSpectator(user)) {return} // not for spectator

		this.reshuffleIfNecessary(user) // TODO : check if there are some case where we don't want this
		if(this.g.policies.length<3) {
			user.roommateExecClient('log',`<red>INTERNAL ERROR</red> : this state should be impossible`)
			console.log("ERROR : this state should be impossible")
		}
		let passed = this.g.policies.pop()
		this.g.policiesAccepted.push(passed)
		this.reshuffleIfNecessary(user)
		user.roommateExecClient('log',`${orangify(user.name)} passed the top policy`)

		// user.roommateExecClient('sfx',`/games/default/alarm.mp3`)
		user.roommateExecClient('sfx',`/games/default/card-play-1.mp3`)
		if(passed == this.policyR) {
			// user.roommateExecClient('sfx',`/games/secret-nigon/enactpolicyf.mp3`)
			user.roommateExecClient('sfx',`/games/default/crowd-shock-1.mp3`)
		} else {
			// user.roommateExecClient('sfx',`/games/secret-nigon/enactpolicyl.mp3`)
			// user.roommateExecClient('sfx',`/games/default/crowd-surprised-1.mp3`)
			user.roommateExecClient('sfx',`/games/default/h2g2-door.mp3`)
		}

		this.checkIfGameEnded()

		user.roommate.map( p => this.show(p))
	}

	presidentVote(user,index) {
		if(this.isSpectator(user)) {return} // not for spectator
		if(index >= this.g.policiesExamined.length) {
			console.log("discard : illegal argument error")
			return
		}
		if(user.name==this.g.president) {
			if(!this.g.chancellor) {
				user.roommateExecClient('log',`<red>INTERNAL ERROR</red> chancellor missing`)
				console.log('INTERNAL ERROR : chancellor missing')
				return
			}
			let toDiscard = this.g.policiesExamined[index]
			this.g.policiesExamined = this.g.policiesExamined.filter((e,i) => i!=index)
			this.g.policiesDropped.push(toDiscard)
			this.g.policiesExaminedBy = this.g.chancellor
			this.g.gameStage = gameStage.chancellorVoting
			// user.roommateExecClient('sfx',`/games/secret-nigon/punch.mp3`)
			// user.roommateExecClient('sfx',`/games/secret-nigon/policy.mp3`)
			user.roommateExecClient('sfx',`/games/default/card-play-1.mp3`)
			user.roommate.map( p => this.show(p))
		}
	}

	checkIfGameEnded() {
		if(this.g.policiesAccepted.filter(p=>p == this.policyB).length == 5) {
			// TODO
			// GENTIL WIN
			this.g.gameStage = gameStage.end
			return true
		}
		if(this.g.policiesAccepted.filter(p=>p == this.policyR).length == 6) {
			// TODO
			// NIGONIST WIN
			this.g.gameStage = gameStage.end
			return true
		}
		return false
	}

	policyVoted(user) {
		let passed = this.g.policiesExamined.pop()

		this.g.policiesAccepted.push(passed)
		this.reshuffleIfNecessary(user)

		if(passed == this.policyR) {
			// user.roommateExecClient('sfx',`/games/secret-nigon/enactpolicyf.mp3`)
			user.roommateExecClient('sfx',`/games/default/crowd-shock-1.mp3`)

			// TODO gameStage could be something else
			let badPolicieCount = this.g.policiesAccepted.filter(p=>p==this.policyR).length
			this.g.gameStage = this.generateBadEvents(this.g.players.length)[badPolicieCount-1]
			// this.g.gameStage = gameStage.presidentChooseChancellor

		} else {
			// user.roommateExecClient('sfx',`/games/secret-nigon/enactpolicyl.mp3`)
			// user.roommateExecClient('sfx',`/games/default/crowd-surprised-1.mp3`)
			user.roommateExecClient('sfx',`/games/default/h2g2-door.mp3`)
			this.g.gameStage = gameStage.presidentChooseChancellor
		}

		if(this.checkIfGameEnded()) {
			user.roommate.map(p => this.show(p))
			return
		}

		if(this.g.gameStage == gameStage.kill||
			this.g.gameStage == gameStage.temporaryPresident||
			this.g.gameStage == gameStage.inspectPolicies||
			this.g.gameStage == gameStage.inspectPlayer ||
			this.g.gameStage == gameStage.veto
		) {
			if(this.g.gameStage == gameStage.inspectPolicies) {
				this.g.policiesExaminedBy = this.g.president
				this.reshuffleIfNecessary(user) // TODO : check if there are some case where we don't want this
				if(this.g.policies.length < 3) {
					user.roommateExecClient('log',`<red>INTERNAL ERROR</red> : this state should be impossible`)
					console.log("ERROR : this state should be impossible")
				} else {
					while(this.g.policiesExamined.length<3) {
						this.g.policiesExamined.push(this.g.policies.pop())
					}
					// user.roommateExecClient('log',`${orangify(user.name)} is <orange>peaking</orange> 3 cards in the deck`)
					// user.roommateExecClient('sfx',`/games/default/alarm.mp3`)
					user.roommateExecClient('sfx',`/games/default/card-take-1.mp3`)
					// user.roommate.map( p => this.show(p))
				}
			}
			// TODO
		} else {
			// TODO ? maybe there is nothing to do left
			this.autoNextPresident(user)
		}

		user.roommate.map(p => this.show(p))

	}

	chancellorVote(user,index) {
		if(this.isSpectator(user)) {return} // not for spectator
		if(index >= this.g.policiesExamined.length) {
			console.log("discard : illegal argument error")
			return
		}
		if(user.name==this.g.chancellor) {
			let toDiscard = this.g.policiesExamined[index]
			this.g.policiesExamined = this.g.policiesExamined.filter((e,i) => i!=index)
			this.g.policiesDropped.push(toDiscard)
			this.g.policiesExaminedBy = undefined

			// user.roommateExecClient('sfx',`/games/secret-nigon/punch.mp3`)
			user.roommateExecClient('sfx',`/games/default/card-play-1.mp3`)

			// TODO :VETO
			let vetoEnabled = this.g.policiesAccepted.filter(p=>p == this.policyR).length == 5
			if(vetoEnabled) {
				this.g.gameStage = gameStage.veto
				user.roommate.map(p => this.show(p))
			} else {
				this.policyVoted(user)
			}

		}
	}

	autoNextPresident(user,TODO_find_a_name_for_this_option = true) {
		let nextNeighbor = (name) => {
			return this.g.players[(this.g.players.indexOf(name)+1) % this.g.players.length]
		}
		let nextLivingNeighbor = (name) => {
			if(this.g.players.map( p => this.g.living[p]).filter(e=>e).length==0) {
				console.log('error : everyone is dead :¬/') // avoid infinite loop
				return
			}
			while(!this.g.living[name]) {
				name = nextNeighbor(name)
			}
			return name
		}

		console.log(TODO_find_a_name_for_this_option)

		if(TODO_find_a_name_for_this_option) {
			this.g.chancellorPrev = this.g.chancellor
		}
		this.g.chancellor = undefined

		// let nextPresident = this.g.temporaryNextPresident || this.g.president
		// if our president died, select his next living neighbor
		let nextPresident = nextLivingNeighbor(this.g.temporaryNextPresident || nextNeighbor(this.g.president))
		this.g.temporaryNextPresident = undefined
		if(TODO_find_a_name_for_this_option) {
			this.setPresident(user,nextPresident)
		} else {
			this.g.president = nextPresident
		}
	}

	jaNein(user,vote) {
		if(this.isSpectator(user)) {return} // not for spectator
		if(vote!='' && vote!='nein' && vote!='ja') {return} // illegal argument

		if(this.g.vote[user.name]==vote) {
			this.g.vote[user.name]=''
		} else {
			this.g.vote[user.name]=vote
		}
		user.roommate.map(p => this.show(p))

		if(global.TESTING) {
			for(let p of this.g.players) this.g.vote[p] = vote
		}

		let alive = this.g.players.filter(p => this.g.living[p])
		let undecided = alive.filter(p => this.g.vote[p] == '')

		if(undecided.length == 0) {
			if(this.g.gameStage == gameStage.crewVoted) {
				return
			}

			this.g.gameStage = gameStage.crewVoted
			user.roommate.map(p => this.show(p,true))

			let duration = 2 + Math.max(0,(alive.length-5)/10) // seconds

			let jaCount = alive.filter(p=>this.g.vote[p]=='ja').length
			let neinCount = alive.filter(p=>this.g.vote[p]=='nein').length

			let nigonCouldWin = this.g.policiesAccepted.filter( p => p==this.policyR ).length >= 3

			if(jaCount>neinCount) {
				// user.roommateExecClient('sfx','/games/default/alarm.mp3')
				// user.roommateExecClient('sfx','/games/default/card-shuffle-1.mp3')
				// user.roommateExecClient('sfx','/games/default/surprise-1.mp3')
				// user.roommateExecClient('sfx','/games/default/suspense-3.mp3')
				if(nigonCouldWin) {
					user.roommateExecClient('sfx','/games/default/suspense-3.mp3')
				} else {
					user.roommateExecClient('sfx','/games/default/suspense-1.mp3')
				}
			} else {
				user.roommateExecClient('sfx','/games/default/suspense-2.mp3')
			}

			setTimeout( () => {
				if(jaCount>neinCount) {
					if(nigonCouldWin && this.g.roles[this.g.chancellor] == this.nigon || this.g.policiesAccepted.filter(p => p == this.policyR).length==6) {
						this.g.gameStage = gameStage.end
					} else {
						this.g.gameStage = gameStage.presidentVoting
						this.g.policiesExaminedBy = this.g.president
						this.reshuffleIfNecessary(user) // TODO : check if there are some case where we don't want this
						if(this.g.policies.length < 3) {
							user.roommateExecClient('log',`<red>INTERNAL ERROR</red> : this state should be impossible`)
							console.log("ERROR : this state should be impossible")
							return
						}
						while(this.g.policiesExamined.length<3) {
							this.g.policiesExamined.push(this.g.policies.pop())
						}
						this.g.failingVote=0
					}

					// TODO
					// user.roommateExecClient('sfx','/games/secret-nigon/crowd-scream-oh-yeah.mp3')
				} else {
					// user.roommateExecClient('sfx','/games/default/suspense-1.mp3')
					this.g.gameStage = gameStage.presidentChooseChancellor
					this.autoNextPresident(user,false)
					this.g.failingVote++
					if(this.g.failingVote==3) {
						this.passTopPolicy(user)
						this.g.failingVote=0
						// now anyone can be voted for
						this.g.presidentPrev=undefined
						this.g.chancellorPrev=undefined
					}
					// TODO pass top policy condition
					// user.roommateExecClient('sfx','/games/secret-nigon/crowd-scream-oh-no.mp3')
				}
				user.roommateExecClient('log',`vote outcome : ${orangify(jaCount+" Ja")} VS ${orangify(neinCount+" Nein")}`)
				for(let p of this.g.players) this.g.vote[p] = ''
				user.roommate.map(p => this.show(p))
			},duration*1000)
		}
	}

	setPresident(user,target) {
		if(this.isSpectator(user)) {return} // not for spectator
		this.g.presidentPrev = this.g.president
		this.g.president = target
		user.roommate.map( p => this.show(p))
	}

	setTemporaryPresident(user,target) {
		if(this.isSpectator(user)) {return} // not for spectator
		this.g.temporaryNextPresident = this.g.players[(this.g.players.indexOf(this.g.president)+1) % this.g.players.length]
		this.g.presidentPrev = this.g.president
		this.g.chancellorPrev = this.g.chancellor
		this.g.chancellor = undefined
		this.g.president = target
		this.g.gameStage = gameStage.presidentChooseChancellor
		user.roommate.map( p => this.show(p))
	}

	setChancellor(user,target) {
		if(this.isSpectator(user)) {return} // not for spectator
		if(this.g.chancellor) this.g.chancellorPrev = this.g.chancellor
		this.g.chancellor = target
		this.g.gameStage = gameStage.crewVoting
		user.roommate.map( p => this.show(p))
	}

	printPlayer(user,p,voteMode=false,canSeeOverride=false,canSeeOverride2=true) {
		let myself = p==user.name

		let isSpectator = !(user.name && user.name.length)
		let isMechant = this.g.roles[user.name] == this.nigoniste
		let isNigonMaisPeuDeJoueurs = this.g.roles[user.name] == this.nigon && this.g.players.length<=6

		let canSee = canSeeOverride2 && (myself || isMechant || isNigonMaisPeuDeJoueurs || this.g.gameStage == gameStage.end || canSeeOverride)

		let classList = []
		if(myself)                                        classList.push("myself")
		if(!this.g.living[p])                             classList.push("dead")
		if(canSee && this.g.roles[p] == this.gentil)      classList.push("gentil")
		if(canSee && this.g.roles[p] == this.nigoniste)   classList.push("mechant")
		if(canSee && this.g.roles[p] == this.nigon) {if(canSeeOverride) classList.push("mechant"); else classList.push("mechant-chef")}
		if(p == this.g.presidentPrev)                     classList.push("president-prev")
		if(p == this.g.president)                         classList.push("president")
		if(p == this.g.chancellorPrev)                    classList.push("chancellor-prev")
		if(p == this.g.chancellor)                        classList.push("chancellor")

		let classListRole = []
		if(p == this.g.presidentPrev)                     classListRole.push("president-prev")
		if(p == this.g.president)                         classListRole.push("president")
		if(p == this.g.chancellorPrev)                    classListRole.push("chancellor-prev")
		if(p == this.g.chancellor)                        classListRole.push("chancellor")

		if(voteMode && this.g.living[p] ) classList.push(this.g.vote[p])

		let voteStatus = ``
		if(!voteMode && this.g.vote[p]) voteStatus = `<voted></voted>`

		let callback = ``
		let cardSfx = ``
		let cardSfxOut = ``
		if(this.g.gameStage == gameStage.presidentChooseChancellor) {

			// 5 players living : presidentPrev can be chosen as chancellor
			let livingLessThan5 = this.g.players.map(p=>this.g.living[p]).filter(l=>l).length<=5

			let canBeVotedFor = p!=user.name &&
			                    this.g.living[p] &&
			                    (p!=this.g.presidentPrev || livingLessThan5) &&
			                    p!=this.g.chancellorPrev
			if(user.name==this.g.president && canBeVotedFor) {
				callback = `onclick="r.execServer('secretNigon.setChancellor','${p}')"`
				classList.push("choosing-chancellor")
				cardSfx = `onmouseenter="sfx('/games/default/card-slide-2.mp3')"`
				// cardSfxOut = `onmouseleave="sfx('/games/default/card-slide-2.mp3')"`
			}
		}

		if(this.g.gameStage == gameStage.kill) {
			// TODO
			let canBeVotedFor = p!=user.name && this.g.living[p]
			if(user.name==this.g.president && canBeVotedFor) {
				callback = `onclick="r.execServer('secretNigon.kill','${p}')"`
				// classList.push("choosing")
				classList.push("gun-pointing")
				cardSfx = `onmouseenter="sfx('/games/default/card-slide-2.mp3')"`
			}
		}

		if(this.g.gameStage == gameStage.inspectPlayer) {
			// TODO
			let canBeVotedFor = p!=user.name && this.g.living[p]
			if(user.name==this.g.president && canBeVotedFor) {
				//.TODO
				callback = `onclick="r.execServer('secretNigon.checkPlayer','${p}')"`
				classList.push("choosing")
				cardSfx = `onmouseenter="sfx('/games/default/card-slide-2.mp3')"`
			}
		}

		if(this.g.gameStage == gameStage.temporaryPresident) {
			let canBeVotedFor = p!=user.name && this.g.living[p]
			if(user.name==this.g.president && canBeVotedFor) {
				callback = `onclick="r.execServer('secretNigon.setTemporaryPresident','${p}')"`
				classList.push("choosing")
				cardSfx = `onmouseenter="sfx('/games/default/card-slide-2.mp3')"`
			}
		}

		let classname = `class="${classList.join(' ')}"`
		let classnameRole = `class="${classListRole.join(' ')}"`

		let content = `<name>${p}</name><role ${classnameRole}>${voteStatus}</role>`

		// let generateSfxSequencer = `window.secretNigonDramaSFXSequencer=window.secretNigonDramaSFXSequencer||sfx_drama_pitch_sequencer()`
		// return `<player onmouseenter="${generateSfxSequencer};sfx('/games/default/surprise-1.mp3',window.secretNigonDramaSFXSequencer)" ${callback} ${classname}>${content}</player>`

		if(this.g.gameStage == gameStage.kill && user.name == this.g.president) {
			let index = 1 + (this.g.players.indexOf(p)%5)
			cardSfx = `onmouseenter="sfx('/games/default/scared-${index}.mp3', () => (Math.random())*5 )"`
		}

		return `<player ${cardSfx} ${cardSfxOut} ${callback} ${classname}>${content}</player>`
	}

	printPolicies(user) {
		let cardSfx = `onmouseenter="sfx('/games/default/card-slide-2.mp3')"`
		let cardSfxOut = `onmouseleave="sfx('/games/default/card-slide-2.mp3')"`
		let policies = ``
		if(this.g.policiesExaminedBy) {
			user.roommateExecClient('sfx',`/games/default/card-take-1.mp3`)
			if(this.g.policiesExaminedBy == user.name) {
				let discardable = ''
				let discardSfx = ''
				let discardCallback = index => ''
				let cancelbutton =``
				if(this.g.gameStage == gameStage.presidentVoting && this.g.president==user.name) {
					discardable = 'discardable'
					discardSfx = `onmouseenter="sfx('/games/default/trash-short.mp3')"`
					discardCallback = index => `onclick="r.execServer('secretNigon.presidentVote',${index})"`
				}
				if(this.g.gameStage == gameStage.chancellorVoting && this.g.chancellor==user.name) {
					discardable = 'discardable'
					discardSfx = `onmouseenter="sfx('/games/default/trash-short.mp3')"`
					discardCallback = index => `onclick="r.execServer('secretNigon.chancellorVote',${index})"`
				}
				if(this.g.gameStage == gameStage.inspectPolicies) {
					let returnCallback = `onclick="r.execServer('secretNigon.returnCards')"`
					cancelbutton = `<cancel ${returnCallback} ${cardSfx} ${cardSfxOut}>return</cancel>`
				}
				policies = `
					<policies>
					${cancelbutton}
					${this.g.policiesExamined.map( (e,i) => `<policy ${discardCallback(i)} class="position-${i+1}-of-3 ${this.g.policiesExamined[i] == this.policyB ? 'gentil': 'mechant'} ${discardable}" ${discardSfx}></policy>`).join('')}
					</policies>`
			} else {
				policies = `<policies><who>${this.g.policiesExaminedBy}</who>${this.g.policiesExamined.map(e=>'<policy></policy>').join('')}</policies>`
			}
		}
		if(this.g.checkedPlayer) {
			if(user.name == this.g.president) {
				user.roommateExecClient('sfx',`/games/default/card-take-1.mp3`)

				let returnCallback = `onclick="r.execServer('secretNigon.checkPlayerEnd')"`
				let cancelbutton = `<cancel ${returnCallback} ${cardSfx} ${cardSfxOut}>return</cancel>`

				// policies = `<policies>${cancelbutton} <div>${this.g.checkedPlayer} is ${ this.g.roles[this.g.checkedPlayer]==this.gentil?'gentil':'mechant'}</div></policies>`
				policies = `<policies>${cancelbutton} ${this.printPlayer(user,this.g.checkedPlayer,false,true)}</policies>`
			} else {
				policies = `<policies><who>${this.g.president}</who> ${this.printPlayer(user,this.g.checkedPlayer,false,false,false)}</policies>`
			}

		}
		return policies
	}

	checkPlayerEnd(user) {
		this.g.checkedPlayer = undefined
		this.autoNextPresident(user)
		this.g.gameStage = gameStage.presidentChooseChancellor
		user.roommate.map( p => this.show(p))
	}

	veto(user,vote) {
		if(this.isSpectator(user)) {return} // not for spectator

		if(user.name == this.g.president) {
			if(this.g.vetoPresident == vote) {
				this.g.vetoPresident = ''
			} else {
				this.g.vetoPresident = vote
			}
		}

		if(user.name == this.g.chancellor) {
			if(this.g.vetoChancellor == vote) {
				this.g.vetoChancellor = ''
			} else {
				this.g.vetoChancellor = vote
			}
		}

		let bothVoted = this.g.vetoChancellor!='' && this.g.vetoPresident!=''
		if(bothVoted) {
			let bothNein = this.g.vetoChancellor=='nein' && this.g.vetoPresident=='nein'
			if(bothNein) {
				this.g.policiesDropped.push(this.g.policiesExamined.pop())
			} else {
				this.policyVoted(user)
			}
			this.g.vetoChancellor = ''
			this.g.vetoPresident = ''
			user.roommateExecClient('sfx',`/games/default/card-play-1.mp3`)
			this.autoNextPresident(user)
			this.g.gameStage = gameStage.presidentChooseChancellor
		}

		user.roommate.map( p => this.show(p))
	}

	constructor(user) {

		this.show = user => user.execClient('gui',``)

		this.start = user => {
			user.roommateExecClient('log',`${orangify(user.name)} is starting secret ${this.nigon}`)
			let playerCount = user.roommateplayer.length

			if(!global.TESTING) {
				if(playerCount<5) {
					user.roommateExecClient('log',`need ${orangify(5-playerCount)} more players to start (min 5 players)`)
					return false
				}
				if(playerCount>10) {
					user.roommateExecClient('log',`${orangify(playerCount-10)} players must leave to start (max 10 players)`)
					return false
				}
			}

			this.g = {}
			this.g.players = shuffle(user.roommateplayer) // the game now freezes players playing
			this.g.roles = {}
			this.g.living = {}
			this.g.vote = {}
			this.g.president
			this.g.chancellor
			this.g.policies = shuffle(this.generatePolicies())
			this.g.policiesExaminedBy
			this.g.policiesExamined = []
			this.g.policiesDropped = []
			this.g.policiesAccepted = []
			this.g.gameStage = gameStage.presidentChooseChancellor
			this.g.failingVote=0
			this.g.vetoPresident=''
			this.g.vetoChancellor=''

			if(global.TESTING) {
				this.g.players = ['laurent','pierre','rosine','christine','vera','adrien']
				// this.g.players = ['laurent','pierre','rosine','christine','vera','adrien','ausra','louis','garance','laura']
				playerCount = this.g.players.length
				// this.g.president = 'laurent'
				// this.g.chancellor = 'pierre'

				// === test veto ===
				// this.g.gameStage = gameStage.veto
				// this.g.policiesExamined.push(this.g.policies.pop())
				// this.g.chancellor = 'pierre'
			}

			let rolePool = shuffle(this.generateRoles(playerCount))
			for(let p of this.g.players) this.g.roles[p] = rolePool.pop()
			for(let p of this.g.players) this.g.living[p] = true
			for(let p of this.g.players) this.g.vote[p] = ''

			this.g.president = this.g.players[0]

			this.show = (user, voteResult=false) => {

				let cardSfx = `onmouseenter="sfx('/games/default/card-slide-2.mp3')"`
				let cardSfxOut = `onmouseleave="sfx('/games/default/card-slide-2.mp3')"`

				let players = `<players>${this.g.players.map( p => this.printPlayer(user,p,voteResult)).join('')}</players>`

				let JaNeinClass = `class="${this.g.vote[user.name]}"`
				let JaNein = ``
				if(this.g.gameStage == gameStage.crewVoting) {
					JaNein = `<JaNein><ja ${JaNeinClass} ${cardSfx} onclick="r.execServer('secretNigon.jaNein','ja')"></ja><nein ${JaNeinClass} ${cardSfx} onclick="r.execServer('secretNigon.jaNein','nein')"></nein></JaNein>`
				}
				if(this.g.gameStage == gameStage.veto) {
					let aboutToBePassed = this.g.policiesExamined[this.g.policiesExamined.length-1]
					user.roommateExecClient('sfx',`/games/default/card-take-1.mp3`)

					let popo = `<policy class="${aboutToBePassed == this.policyB ? 'gentil': 'mechant'}"></policy>`
					if(user.name == this.g.president) {
						// TODO show what is being voted
						JaNeinClass = `class="${this.g.vetoPresident}"`
						JaNein = `${popo}<JaNein><ja ${JaNeinClass} ${cardSfx} onclick="r.execServer('secretNigon.veto','ja')"></ja><nein ${JaNeinClass} ${cardSfx} onclick="r.execServer('secretNigon.veto','nein')"></nein></JaNein>`
						JaNein = JaNein + `<div>ja = accept this policy<br>nein = refuse this policy</div>`
						JaNein = `<veto>${JaNein}</veto>`
					} else if(user.name == this.g.chancellor) {
						// TODO show what is being voted
						JaNeinClass = `class="${this.g.vetoChancellor}"`
						JaNein = `${popo}<JaNein><ja ${JaNeinClass} ${cardSfx} onclick="r.execServer('secretNigon.veto','ja')"></ja><nein ${JaNeinClass} ${cardSfx} onclick="r.execServer('secretNigon.veto','nein')"></nein></JaNein>`
						JaNein = JaNein + `<div>ja = accept this policy<br>nein = refuse this policy</div>`
						JaNein = `<veto>${JaNein}</veto>`
					} else {
						JaNein = `<policies><div style="margin:auto;color:rgba(0,0,0,.8);font-size:30px;">VETO</div><who>${this.g.president}</who> <who>${this.g.chancellor}</who></policies>`
					}
				}

				let policies = this.printPolicies(user)

				let f_president = "r.execServer('secretNigon.setPresident',this.value)"
				let f_chancellor = "r.execServer('secretNigon.setChancellor',this.value)"

				let vote = (JaNein || policies) ?`<vote>${JaNein}${policies}</vote>`:''

				let policiesCount = this.g.policies.length
				let policiesDroppedCount = this.g.policiesDropped.length
				let deck = `<deck><in>${range(policiesCount).fill("<loi></loi>").join('')}</in><count>x${policiesCount}</count><out>${range(policiesDroppedCount).fill("<loi></loi>").join('')}</out><count>x${policiesDroppedCount}</count></deck>`

				let gentilCount  = this.g.policiesAccepted.filter( p => p == this.policyB).length
				let mechantCount = this.g.policiesAccepted.filter( p => p == this.policyR).length
				let boardClass
				switch(playerCount) {
					case 5: case 6: boardClass = 'bottom-1'; break
					case 7: case 8: boardClass = 'bottom-2'; break
					case 9: case 10: boardClass = 'bottom-3'; break
				}

				let votingPhase = this.g.gameStage==gameStage.crewVoting ||
				                  this.g.gameStage==gameStage.presidentVoting ||
				                  this.g.gameStage==gameStage.chancellorVoting ||
				                  this.g.gameStage==gameStage.inspectPolicies ||
				                  this.g.gameStage==gameStage.inspectPlayerPhase2 ||
				                  this.g.gameStage==gameStage.veto ||
				                  this.g.gameStage==gameStage.crewVoted
				let board = `<board-container>${vote}<board class=${votingPhase?'voting':''}>
				<half class='top'>
					${range(gentilCount).fill(`<card ${cardSfx}></card>`).join('')}
					${range(5-gentilCount).fill('<div></div>').join('')}
					${deck}<pass-top-policy></pass-top-policy>
					<fail class="pos-${this.g.failingVote+1}"></fail>
				</half>
				<half class='${boardClass}'>${range(mechantCount).fill(`<card ${cardSfx}></card>`).join('')}
				</half></board></board-container>`

				user.execClient('gui',`<secret-nigon>${players}${board}</secret-nigon>`)

				let nazify = txt => `<span style="font-family:'Eskapade';">${txt}</span>`
				switch(this.g.gameStage) {
					       case gameStage.start:                     user.execClient('gui-top-one-liner',nazify(``))
					break; case gameStage.presidentChooseChancellor: user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.president)} must choose a chancellor`))
					break; case gameStage.crewVoting:                user.execClient('gui-top-one-liner',nazify(`you all have to vote`))
					break; case gameStage.crewVoted:                 user.execClient('gui-top-one-liner',nazify(``))
					break; case gameStage.presidentVoting:           user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.president)} has to vote policy`))
					break; case gameStage.chancellorVoting:          user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.chancellor)} has to vote policy`))
					break; case gameStage.temporaryPresident:        user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.president)} has to choose the president for the next round`))
					break; case gameStage.inspectPolicies:           user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.president)} is inspecting policies`))
					break; case gameStage.inspectPlayer:             user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.president)} has to inspect one player`))
					break; case gameStage.inspectPlayerPhase2:       user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.president)} is inspecting ${orangify(this.g.checkedPlayer)}`))
					break; case gameStage.veto:                      user.execClient('gui-top-one-liner',nazify(`VETO`))
					break; case gameStage.kill:                      user.execClient('gui-top-one-liner',nazify(`${orangify(this.g.president)} has to kill someone`))
					break; case gameStage.end:                       user.execClient('gui-top-one-liner',nazify(`GAME ENDED`))
				}

				if(this.g.gameStage == gameStage.end) {
					let mechantWin = false
					// nigon chancellor with 3 policies
					mechantWin = mechantWin || this.g.policiesAccepted.filter( p => p==this.policyR ).length >= 3 && this.g.roles[this.g.chancellor]==this.nigon
					// 6 policies
					mechantWin = mechantWin || this.g.policiesAccepted.filter( p => p==this.policyR ).length == 6
					if(mechantWin) {
						user.roommateExecClient('sfx','/games/default/thunder-1.mp3')
					} else {
						user.roommateExecClient('sfx','/games/default/windows-3.1.mp3')
					}
					// user.roommateExecClient('sfx','/games/default/suspense-3.mp3')
				}
			}

			user.roommate.map( p => this.show(p))
			user.roommateExecClient('log',`secret nigon started with ${this.g.players.map(p => orangify(p)).join(', ')}`)
			return true
		}
	}
}

exports.SecretNigon = SecretNigon
