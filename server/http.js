let http = require('http')

let fs = require('fs')
let path = require('path')
let mime = require('mime-types')
let url = require('url')

let logger = require('./logger.js')
let col = require('./consoleColor.js')

let log = msg => logger.log(`${col.y}http ${col.no}${msg}`)
let err = msg => logger.err(`${col.y}http ${col.no}${msg}`)

let viewAsDir = function(fullpath,pathname) {
	return `<pre>${fs.readdirSync(fullpath).map(f => `<a href="${pathname}/${encodeURI(f)}?dir=1">${f}</a>`).join('\n')}</pre>`
}

let resFile = (res,filePath) => {
	res.writeHead(200, { "Content-Type": mime.lookup(filePath) })
	fs.createReadStream(filePath).pipe(res)
}
let resHtml = (res,html) => {
	res.writeHead(200, { "Content-Type": mime.lookup('index.html') })
	res.write(html)
	res.end()
}

let createHTTPServer = (port) => {
	return http.createServer()
	.listen(port, () => log("started. port " + port))
	.on('request',(req, response) => {

		let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress

		let pathnameReq = decodeURI(url.parse(req.url).pathname)
		let pathname = "/"+pathnameReq.split("/").filter(e=>e.length).join('/')

		// clean up path
		if(pathname!=pathnameReq) {
			response.writeHead(301,{Location: pathname})
			response.end()
			return
		}

		// /room/* → /room/room.html
		if(pathname.substring(0,"/room/".length)=="/room/") {
			pathname = "/room/room.html"
		}

		let fullpath = __dirname + '/../www' + pathname

		let exists = fs.existsSync(fullpath)
		let isDir = exists && fs.lstatSync(fullpath).isDirectory()
		let forceDir = isDir && url.parse(req.url,true).query.dir
		let isHtml = isDir && fs.existsSync(fullpath + '/index.html')

		if(exists) {
			if(forceDir) {
				resHtml(response,viewAsDir(fullpath,pathname))
				return
			}
			if(!isDir) {
				resFile(response,fullpath)
				return
			}
			if(isHtml) {
				log( `${ip}${col.y} ${pathname}${col.no}`)
				resFile(response,fullpath+'/index.html')
				return
			}
			if(isDir) {
				resHtml(response,viewAsDir(fullpath,pathname))
				return
			}
		}
		err(`${ip} ${col.y}${pathname}${col.r} → 404${col.no}`)
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("ERROR File does not exist")
	})
}

exports.createHTTPServer = createHTTPServer
