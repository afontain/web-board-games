
exports.no = exports.normal     = (txt) => `\x1b[0m${txt}\x1b[0m`
exports.br = exports.bright     = (txt) => `\x1b[1m${txt}\x1b[0m`
exports.di = exports.dim        = (txt) => `\x1b[2m${txt}\x1b[0m`
exports.un = exports.underscore = (txt) => `\x1b[4m${txt}\x1b[0m`
exports.bl = exports.blink      = (txt) => `\x1b[5m${txt}\x1b[0m`
exports.re = exports.reverse    = (txt) => `\x1b[7m${txt}\x1b[0m`
exports.hi = exports.hidden     = (txt) => `\x1b[8m${txt}\x1b[0m`
exports.k  = exports.black      = (txt) => `\x1b[30m${txt}\x1b[0m`
exports.r  = exports.red        = (txt) => `\x1b[31m${txt}\x1b[0m`
exports.g  = exports.green      = (txt) => `\x1b[32m${txt}\x1b[0m`
exports.y  = exports.yellow     = (txt) => `\x1b[33m${txt}\x1b[0m`
exports.b  = exports.blue       = (txt) => `\x1b[34m${txt}\x1b[0m`
exports.m  = exports.magenta    = (txt) => `\x1b[35m${txt}\x1b[0m`
exports.c  = exports.cyan       = (txt) => `\x1b[36m${txt}\x1b[0m`
exports.w  = exports.white      = (txt) => `\x1b[37m${txt}\x1b[0m`
exports.K  = exports.black_bg   = (txt) => `\x1b[40m${txt}\x1b[0m`
exports.R  = exports.red_bg     = (txt) => `\x1b[41m${txt}\x1b[0m`
exports.G  = exports.green_bg   = (txt) => `\x1b[42m${txt}\x1b[0m`
exports.Y  = exports.yellow_bg  = (txt) => `\x1b[43m${txt}\x1b[0m`
exports.B  = exports.blue_bg    = (txt) => `\x1b[44m${txt}\x1b[0m`
exports.M  = exports.magenta_bg = (txt) => `\x1b[45m${txt}\x1b[0m`
exports.C  = exports.cyan_bg    = (txt) => `\x1b[46m${txt}\x1b[0m`
exports.W  = exports.white_bg   = (txt) => `\x1b[47m${txt}\x1b[0m`

// exports.br = exports.bright     = (txt) => `\x1b[1m${txt}\x1b[0m`
// exports.un = exports.underscore = (txt) => `\x1b[4m${txt}\x1b[0m`
// exports.bl = exports.blink      = (txt) => `\x1b[5m${txt}\x1b[0m`
// exports.re = exports.reverse    = (txt) => `\x1b[7m${txt}\x1b[0m`

exports.k_dim  = exports.black_dim  = (txt) => `\x1b[2m\x1b[30m${txt}\x1b[0m`
exports.r_dim  = exports.red_dim    = (txt) => `\x1b[2m\x1b[31m${txt}\x1b[0m`
exports.g_dim  = exports.green_dim  = (txt) => `\x1b[2m\x1b[32m${txt}\x1b[0m`
exports.y_dim  = exports.yellow_dim = (txt) => `\x1b[2m\x1b[33m${txt}\x1b[0m`
exports.b_dim  = exports.blue_dim   = (txt) => `\x1b[2m\x1b[34m${txt}\x1b[0m`
exports.m_dim  = exports.magenta_dim= (txt) => `\x1b[2m\x1b[35m${txt}\x1b[0m`
exports.c_dim  = exports.cyan_dim   = (txt) => `\x1b[2m\x1b[36m${txt}\x1b[0m`
exports.w_dim  = exports.white_dim  = (txt) => `\x1b[2m\x1b[37m${txt}\x1b[0m`
