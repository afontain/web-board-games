let col = require('./consoleColor2.js')
let logger = require('./logger.js')
let shuffle = require('./games/tools.js').shuffle
let TimeBomb = require('./games/timebomb.js').TimeBomb
let SecretNigon = require('./games/secret-nigon.js').SecretNigon
// let SecretNigon = require('./games/super-secret-nigon.js').SecretNigon
let Coup = require('./games/coup.js').Coup
let Midi = require('./games/midi.js').Midi
let Default = require('./games/default.js').Default

let properString = require('./games/tools.js').properString
let orangify = str => '<orange>'+str+'</orange>'

// ==============================

class Room {
	static all = {}
	static get(roomName) {
		if(!roomName) return null
		Room.all[roomName] = Room.all[roomName] || new Room(roomName)
		return Room.all[roomName]
	}

	#games = {}
	get timebomb()    { return this.#games['timebomb']    = this.#games['timebomb']    || new TimeBomb() }
	get secretNigon() { return this.#games['secretNigon'] = this.#games['secretNigon'] || new SecretNigon() }
	get coup()        { return this.#games['coup']        = this.#games['coup']        || new Coup() }
	get midi()        { return this.#games['midi']        = this.#games['midi']        || new Midi() }
	get default()     { return this.#games['default']     = this.#games['default']     || new Default() }

	constructor(name) {
		this.name = name
		this.currentGame = this.default
	}
}

class User {
	static all = []
	get roommate()       {return User.all.filter(u => u.room==this.room)} // include spectators
	get roommateplayer() {return this.roommate.filter(u => u.name).map(u => u.name).filter((v, i, a) => a.indexOf(v) === i)}// exclude spectators and duplicate
	get roommatepeople()   {return this.roommate.map(u=>u.name||`[spectator]`).sort()}
	roommateExecClient(exec,args) {this.roommate.map(u=>u.execClient(exec,args))}

	execClient(exec,args) {this.#connection.send(JSON.stringify({exec:exec,args:args}))}

	#connection

	listener = {}
	listen(actionName,callback) {
		this.listener[actionName] = this.listener[actionName] || []
		this.listener[actionName].push(callback)
	}

	constructor(wsServerReq) {
		this.#connection = wsServerReq.accept(null, wsServerReq.origin)
		this.logFile = msg => logger.logFile(`${col.b('wsok')} [${col.b(wsServerReq.key)}${(this.room?`|${col.g(this.room)}`:"")}${(this.name?(`|${col.y(this.name)}`):"")}] ${msg}`)
		this.log     = msg => logger.log    (`${col.b('wsok')} [${col.b(wsServerReq.key)}${(this.room?`|${col.g(this.room)}`:"")}${(this.name?(`|${col.y(this.name)}`):"")}] ${msg}`)
		User.all.push(this)
		this.log('connected')

		this.#connection.on('message', msg => {
			msg = JSON.parse(msg.utf8Data)
			if(this.listener[msg.exec]) {
				this.logFile(JSON.stringify(msg))
				this.listener[msg.exec].map( callback => callback(msg.args))
			}
		})

		// ==============================
		// == login logout

		let login = name => {
			let oldname = this.name
			this.name = properString(name||'').substr(0,9)
			if(oldname == this.name) return
			if(!oldname && this.name) {
				this.log(`login`)
				this.roommateExecClient('log',`${orangify(this.name)} logged in`)
			}
			if(oldname && this.name) {
				this.log(`login ${col.y_dim(oldname)} → ${col.y(this.name)}`)
				this.roommateExecClient('log',`change user ${orangify(oldname)} → ${orangify(this.name)}`)
			}
			if(oldname && !this.name) {
				this.log(`logout`)
				this.roommateExecClient('log',`${orangify(oldname)} left`)
			}
			this.roommateExecClient('event.room.list',this.roommatepeople)
			this.execClient('return.login',this.name)
		}

		this.#connection.on('close', connection => {
			User.all = User.all.filter( e => e!=this )
			this.log( "disconnected" )
			login() // means logout
		})

		this.listen('login', args => login(args))

		// ==============================
		// == room

		this.listen('room', args => {
			this.room = args.room

			let currentRoom = Room.get(this.room)

			if(args.exec=='show') currentRoom.currentGame.show(this)

			if(args.exec=='say') {
				this.roommateExecClient('sfx',`/games/default/msn-messenger-notif.mp3`)
				// TODO : check if HTML ill formatted (auto-close all tags)
				this.roommateExecClient('log',`${orangify(this.name)} : <say>${args.args}</say>`)
			}

			if(args.exec=='wizz') {
				this.roommateExecClient('wizz')
				// TODO : stopper les gens qui abusent
				this.roommateExecClient('log',`${orangify(this.name)} wizz`)
			}

			if(args.exec=='restart') currentRoom.currentGame.start(this)
			if(args.exec=='default.start') if(currentRoom.default.start(this)) currentRoom.currentGame = currentRoom.default

			if(args.exec=='timebomb.start') if(currentRoom.timebomb.start(this)) currentRoom.currentGame = currentRoom.timebomb
			if(args.exec=='timebomb.show')     currentRoom.timebomb.show(this)
			if(args.exec=='timebomb.cut'  )    currentRoom.timebomb.cut(this,args.args)

			if(args.exec=='secretNigon.start')      if(currentRoom.secretNigon.start(this)) currentRoom.currentGame = currentRoom.secretNigon
			if(args.exec=='secretNigon.show')          currentRoom.secretNigon.show(this)
			if(args.exec=='secretNigon.jaNein')        currentRoom.secretNigon.jaNein(this,args.args)
			if(args.exec=='secretNigon.veto')          currentRoom.secretNigon.veto(this,args.args)
			if(args.exec=='secretNigon.kill')          currentRoom.secretNigon.kill(this,args.args)
			if(args.exec=='secretNigon.peak')          currentRoom.secretNigon.peak(this,args.args)
			if(args.exec=='secretNigon.discard')       currentRoom.secretNigon.discard(this,args.args)
			if(args.exec=='secretNigon.setPresident')  currentRoom.secretNigon.setPresident(this,args.args)
			if(args.exec=='secretNigon.setChancellor') currentRoom.secretNigon.setChancellor(this,args.args)
			if(args.exec=='secretNigon.passTopPolicy') currentRoom.secretNigon.passTopPolicy(this,args.args)
			if(args.exec=='secretNigon.presidentVote') currentRoom.secretNigon.presidentVote(this,args.args)
			if(args.exec=='secretNigon.chancellorVote')currentRoom.secretNigon.chancellorVote(this,args.args)
			if(args.exec=='secretNigon.setTemporaryPresident')currentRoom.secretNigon.setTemporaryPresident(this,args.args)
			if(args.exec=='secretNigon.returnCards')   currentRoom.secretNigon.returnCards(this,args.args)
			if(args.exec=='secretNigon.checkPlayer')   currentRoom.secretNigon.checkPlayer(this,args.args)
			if(args.exec=='secretNigon.checkPlayerEnd')currentRoom.secretNigon.checkPlayerEnd(this,args.args)

			if(args.exec=='coup.start')     if(currentRoom.coup.start(this)) currentRoom.currentGame = currentRoom.coup

			if(args.exec=='midi.start')     if(currentRoom.midi.start(this)) currentRoom.currentGame = currentRoom.midi
			if(args.exec=='midi.shareEvent')currentRoom.midi.shareEvent(this,args.args)

		})

		// this.listen('roomlist', args => user.execClient('log',getRooms()))
		// this.listen('list', args => this.execClient('return.list',allUsernames()))

		// this.execClient("event.list",allUsernames())
		// this.execClient("event.roomlist",[])
		this.execClient("event.room.list",this.roommatepeople)

	}
}

exports.User = User
exports.Room = Room
