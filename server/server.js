"use strict"

// ================
// init + args

process.title = 'web-board-game'
let port = parseInt(process.argv[2]) || 8081

// ============
// dependencies

let createHTTPServer = require("./http.js").createHTTPServer
let webSocketServer = require('websocket').server
let {User,Room} = require("./user.js")

// ============
// server setup

let server = createHTTPServer(port)

let wsServer = new webSocketServer({httpServer: server})

wsServer.on('request', request => {

	let user = new User(request)

})

// global.TESTING = true

let repl = require('repl').start('> ')
repl.context.User = User
repl.context.Room = Room
